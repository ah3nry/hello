  const callback = () => {
    document.body.style.setProperty(
        "--scroll",
        window.pageYOffset / (document.body.offsetHeight - window.innerHeight)
    );
  }
  
  window.addEventListener(
    "scroll",
    callback,
    false
  );